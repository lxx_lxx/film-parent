package com.api.cinema;

import com.movie.entity.Cinema;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Component
@FeignClient("movie-cinema")
@RequestMapping("/cinema")
public interface CinemaClients {

    @RequestMapping("/findCinemaDetail")
    List<Cinema> findCinemaDetail(@RequestParam("cinema_id") Integer cinema_id);

}
