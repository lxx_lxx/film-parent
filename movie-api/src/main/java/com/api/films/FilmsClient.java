package com.api.films;

import com.movie.vo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Component
@FeignClient("movie-films")
@RequestMapping("/films")
public interface FilmsClient {
    //关键字es分页搜索电影
    @PostMapping("/movieKeywordSearch")
    public Result movieKeywordSearch(@RequestBody Map searchMap);
}
