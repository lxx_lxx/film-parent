package com.api.movie;

import com.movie.entity.Movie;
import com.movie.vo.PageResultVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Component
@FeignClient("movie-movie")
@RequestMapping("/movie")
public interface MovieClients {

    @RequestMapping("/queryHotMovie")
    public PageResultVO queryHotMovie(@RequestParam("state") String state);

    @RequestMapping("/queryUploadMovie")
    public List<Movie> queryUploadMovie();

    @RequestMapping("/queryPopularMovie")
    public List<Movie> queryPopularMovie();
}
