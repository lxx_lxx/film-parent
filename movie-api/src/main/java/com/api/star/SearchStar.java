package com.api.star;

import com.movie.vo.PageResultVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@FeignClient("movie-films")
@Component
@RequestMapping("/star")
public interface SearchStar {

    @RequestMapping("/getAllStar")
    public PageResultVO getAllStar();
}
