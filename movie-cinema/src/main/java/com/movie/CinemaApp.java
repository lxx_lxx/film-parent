package com.movie;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author xsl
 * @Date 2021/4/13 22:32
 * @Version 1.0
 **/
@SpringBootApplication
@MapperScan("com.movie.mapper")
@EnableDiscoveryClient
public class CinemaApp {
    public static void main(String[] args) {
        SpringApplication.run(CinemaApp.class,args);
    }

}
