package com.movie.controller;


import com.movie.entity.Cinema;
import com.movie.entity.ESCinema;
import com.movie.entity.Movie;
import com.movie.service.ICinemaService;
import com.movie.vo.PageResultVO;
import com.movie.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xsl
 * @since 2021-04-14
 */
@RestController
@RequestMapping("/cinema")
public class CinemaController {
    @Autowired
    private ICinemaService iCinemaService;

    @RequestMapping("findCinemaInfo")
    public Result findCinemaInfo() {
        return iCinemaService.findCinemaInfo();
    }

    @RequestMapping("search")
    public PageResultVO<ESCinema> search(@RequestBody Map searchMap) {

        return iCinemaService.search(searchMap);
    }

    @RequestMapping("auditCinema")
    public Result auditCinema(Integer cinema_id) {
        return iCinemaService.auditCinema(cinema_id);
    }

    @RequestMapping("findCinemaDetail")
    public List<Cinema> findCinemaDetail(Integer cinema_id) {
        return iCinemaService.findCinemaDetail(cinema_id);
    }

    @RequestMapping("findMovie")
    public List<Movie> findMovie(Integer cinema_id){
        return iCinemaService.findMovie(cinema_id);
    }


}
