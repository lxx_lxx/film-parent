package com.movie.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
//indexName索引库名称
//shards索引库的分片数
//replicas每片的副本数
@Document(indexName = "es-cinema",  shards = 1, replicas = 0)
public class ESCinema {
    //_id不给值es默认生成
    //@Id  id属性值映射给_id
    @Id
    private String id;

    private Integer cinema_id;                         // 影院spuId

    //type 默认值FieldType.Auto【根据属性的java类型自动确定es文档的域类型】
    //index 默认值true   如果域参与搜索、过滤、排序、聚合 index必须为true，否则是false 比如商品图片url
    //store 默认值false  当该域不需要在客户端显示，可以将 store设置为false 比如：商品详情信息
    //analyzer 默认值为“”  如果域需要中文分词，那么analyzer="ik_max_word"

    @Field(type=FieldType.Integer,index=true,store=true)
    private Integer city_id;                       // 城市id
    // FieldType.Keyword 是不会分词的，如果需要分词那么FieldType.Text【默认会分词】
    @Field(type = FieldType.Keyword,index=true,store=true)
    private String city_name;                   // 城市名称

    @Field(type=FieldType.Integer,index=true,store=true)
    private Integer cinemabrand_id;                       // 品牌id
    // FieldType.Keyword 是不会分词的，如果需要分词那么FieldType.Text【默认会分词】
    @Field(type = FieldType.Text,index=true,store=true,analyzer = "ik_max_word")
    private String cinemabrand_name;                   //品牌名称


    @Field(type = FieldType.Keyword,index=true,store=true)
    private String cinema_name;                    // 影院名称

    @Field(type=FieldType.Integer,index=true,store=true)
    private Integer region_id;                        // 区id
    @Field(type = FieldType.Keyword,index=true,store=true)
    private String region_name;                    // 区名称


    @Field(type=FieldType.Integer,index=true,store=true)
    private Integer hall_id;                        // 厅id
    @Field(type = FieldType.Keyword,index=true,store=true)
    private String hall_type;   //CGS中国巨幕厅 ,杜比全景声厅                 //厅名称

                    // 创建时间
    @Field(type=FieldType.Double,index=true,store=true)
    private Double price;                       // 票价格
    @Field(type = FieldType.Text,index = false,store=true,analyzer = "ik_max_word")
    private String address;                    // 影院地址
}