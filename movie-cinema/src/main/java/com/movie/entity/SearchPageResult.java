package com.movie.entity;

import com.movie.vo.PageResultVO;
import lombok.Data;

import java.util.List;

/**
 * @Author xsl
 * @Date 2021/4/15 9:59
 * @Version 1.0
 **/
@Data
public class SearchPageResult extends PageResultVO {
    //品牌聚合信息
    private List<String> cinemabrandNameList;

    //区
    private List<String> regionNameList;

    //厅
    private List<String> hallTypeList;

}
