package com.movie.mapper;

import com.movie.entity.Cinema;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.movie.entity.City;
import com.movie.entity.Movie;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xsl
 * @since 2021-04-14
 */
public interface CinemaMapper extends BaseMapper<Cinema> {
    //影院内容查询
    List<Cinema> findCinemaInfo();

    //根据影院id查询电影
    List<Movie> findMovie(Integer cinema_id);
}
