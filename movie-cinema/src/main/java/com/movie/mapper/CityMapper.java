package com.movie.mapper;

import com.movie.entity.City;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.movie.vo.Result;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xsl
 * @since 2021-04-14
 */
public interface CityMapper extends BaseMapper<City> {



}
