package com.movie.mapper;

import com.movie.entity.Hall;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xsl
 * @since 2021-04-14
 */
public interface HallMapper extends BaseMapper<Hall> {

}
