package com.movie.service;

import com.movie.entity.Cinema;
import com.baomidou.mybatisplus.extension.service.IService;
import com.movie.entity.ESCinema;
import com.movie.entity.Movie;
import com.movie.vo.PageResultVO;
import com.movie.vo.Result;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xsl
 * @since 2021-04-14
 */
public interface ICinemaService extends IService<Cinema> {
    //同步索引库
    Result findCinemaInfo();

    //影院站内搜索
    PageResultVO<ESCinema> search(Map searchMap);

    //RocketMQ发送影院id 生成静态化
    Result auditCinema(Integer cinema_id);

    //查询影院详情
    List<Cinema> findCinemaDetail(Integer cinema_id);

    //根据影院id查询电影
    List<Movie> findMovie(Integer cinema_id);
}
