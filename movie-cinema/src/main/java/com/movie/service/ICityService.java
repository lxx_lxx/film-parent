package com.movie.service;

import com.movie.entity.Cinema;
import com.movie.entity.City;
import com.baomidou.mybatisplus.extension.service.IService;
import com.movie.vo.Result;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xsl
 * @since 2021-04-14
 */
public interface ICityService extends IService<City> {

}
