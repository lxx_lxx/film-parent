package com.movie.service;

import com.movie.entity.Movie;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xsl
 * @since 2021-04-14
 */
public interface IMovieService extends IService<Movie> {

}
