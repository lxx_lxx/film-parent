package com.movie.service.impl;

import com.movie.entity.Cinemabrand;
import com.movie.mapper.CinemabrandMapper;
import com.movie.service.ICinemabrandService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xsl
 * @since 2021-04-14
 */
@Service
public class CinemabrandServiceImpl extends ServiceImpl<CinemabrandMapper, Cinemabrand> implements ICinemabrandService {

}
