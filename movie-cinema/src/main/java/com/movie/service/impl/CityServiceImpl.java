package com.movie.service.impl;

import com.movie.entity.*;
import com.movie.mapper.CityMapper;
import com.movie.service.ICityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.movie.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xsl
 * @since 2021-04-14
 */
@Service
public class CityServiceImpl extends ServiceImpl<CityMapper, City> implements ICityService {

}
