package com.movie.service.impl;

import com.movie.entity.Region;
import com.movie.mapper.RegionMapper;
import com.movie.service.IRegionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xsl
 * @since 2021-04-14
 */
@Service
public class RegionServiceImpl extends ServiceImpl<RegionMapper, Region> implements IRegionService {

}
