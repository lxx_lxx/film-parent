package com.movie.mapper;

import com.movie.entity.CityHall;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lxx
 * @since 2021-04-13
 */
public interface CityHallMapper extends BaseMapper<CityHall> {

}
