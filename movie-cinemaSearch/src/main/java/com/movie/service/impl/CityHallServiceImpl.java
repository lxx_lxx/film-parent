package com.movie.service.impl;

import com.movie.entity.CityHall;
import com.movie.mapper.CityHallMapper;
import com.movie.service.ICityHallService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lxx
 * @since 2021-04-13
 */
@Service
public class CityHallServiceImpl extends ServiceImpl<CityHallMapper, CityHall> implements ICityHallService {

}
