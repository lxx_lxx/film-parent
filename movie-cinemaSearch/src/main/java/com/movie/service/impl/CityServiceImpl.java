package com.movie.service.impl;

import com.movie.entity.City;
import com.movie.mapper.CityMapper;
import com.movie.service.ICityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lxx
 * @since 2021-04-13
 */
@Service
public class CityServiceImpl extends ServiceImpl<CityMapper, City> implements ICityService {

}
