package com.movie.service.impl;

import com.movie.entity.Hall;
import com.movie.mapper.HallMapper;
import com.movie.service.IHallService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lxx
 * @since 2021-04-13
 */
@Service
public class HallServiceImpl extends ServiceImpl<HallMapper, Hall> implements IHallService {

}
