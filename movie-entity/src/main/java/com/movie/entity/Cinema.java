package com.movie.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxx
 * @since 2021-04-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Cinema implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "cinema_id", type = IdType.AUTO)
    private Integer cinemaId;

    private String cinemaName;

    private String pic;

    private String address;

    /**
     * 座机电话
     */
    private String phone;

    //退票信息
    private String re_ticket;
    //改票信息
    private String change_ticket;
    //3D眼镜
    private String glasses;
    //儿童票
    private String children;
    //wifi信息
    private String wifi;



    private Integer cinemabrandId;

    private Integer regionId;

    //城市
    @TableField(exist = false)
    private City city;

    //区
    @TableField(exist = false)
    private Region region;

    //影城
    @TableField(exist = false)
    private Cinemabrand cinemabrand;

    //厅
    @TableField(exist = false)
    private Hall hall;

    //电影
    @TableField(exist = false)
    private Movie movie;




}
