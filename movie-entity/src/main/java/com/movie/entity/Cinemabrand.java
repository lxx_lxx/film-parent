package com.movie.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
<<<<<<< HEAD
 * @author xsl
 * @since 2021-04-14
=======
 * @author lxx
 * @since 2021-04-13
>>>>>>> b3dd6f867d6bfb814e6f18eea52275af7b118554
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Cinemabrand implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "cinemabrand_id", type = IdType.AUTO)
    private Integer cinemabrandId;

    private String cinemabrandName;

    private Integer cityId;


}
