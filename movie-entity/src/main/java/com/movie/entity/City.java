package com.movie.entity;

import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxx
 * @since 2021-04-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class City implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "city_id", type = IdType.AUTO)
    private Integer cityId;

    private String cityName;

    private String firstLetter;

    private Integer isHot;

    private Integer state;

}
