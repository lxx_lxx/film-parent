package com.movie.entity;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxx
 * @since 2021-04-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CityHall implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer cinemaId;

    private Integer hallId;


}
