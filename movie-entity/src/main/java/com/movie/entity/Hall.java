package com.movie.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = false)
public class Hall implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "hall_id", type = IdType.AUTO)
    private Integer hallId;

    private String hallName;

    /**
     * 电影厅类型说明
     */
    private String hallType;

    /**
     * 电影厅座位数
     */
    private Integer seatNum;
    private Integer cinemaId;

    private Integer hullNum;


}
