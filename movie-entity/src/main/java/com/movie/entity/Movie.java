package com.movie.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
<<<<<<< HEAD
 * @author yjc
=======
 * @author lxx
>>>>>>> f5fb2a12c1ab10d37bcbd8ae8ab1cfc500f61a1f
 * @since 2021-04-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Movie implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 电影编号
     */
    @TableId(value = "movie_id", type = IdType.AUTO)
    private Integer movieId;

    /**
     * 电影名称
     */
    private String movieName;

    /**
     * 电影图片
     */
    private String moviePic;

    /**
     * 0:热映，1：即将上映
     */
    private String state;

    /**
     * 电影单价
     */
    private Double price;

    /**
     * 电影评分
     */
    private String score;

    /**
     * 电影简介
     */
    private String introduceId;

    /**
     * 上映时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date uploadTime;

    /**
     * 电影时长
     */
    private Integer totalTime;


    /**
     * 主演
     */
    private String mainstar;

    /**
     * 电影类型
     */
    @TableField(exist = false)
    private  List<Type> typeList;

    @TableField(exist = false)
    private Star star;
    @TableField(exist = false)
    private Type type;
    @TableField(exist = false)
    private Country country;

    private Double ticket;

    private Integer wantNum;


    /**
     * lxx静态页面渲染数据
     */
    @TableField(exist = false)
    private List<Star> starList;
    @TableField(exist = false)
    private MovieIntroduce introduce;
    @TableField(exist = false)
    private Star director;//导演
}
