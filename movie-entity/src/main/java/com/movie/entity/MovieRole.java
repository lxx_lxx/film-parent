package com.movie.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieRole {

  private long roleId;
  private String roleName;
  private long starId;
  private long movieId;



}
