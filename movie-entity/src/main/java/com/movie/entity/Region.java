package com.movie.entity;

import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xsl
 * @since 2021-04-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Region implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "region_id", type = IdType.AUTO)
    private Integer regionId;

    /**
     * 地区名称
     */
    private String regionName;

    /**
     * 父地区ID
     */
    private Integer cityId;


}
