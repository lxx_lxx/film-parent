package com.movie.entity;

import java.util.Date;
import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxx
 * @since 2021-04-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Star implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer starId;

    private String starName;

    private String sex;

    /**
     * 个人介绍
     */
    private String introduce;

    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date birth;

    /**
     * 0:演员 1：导演 2：编剧
     */
    private String type;

    /**
     * 国籍
     */
    private String nationality;

    private Integer fensNum;

    private String englishName;

    private String starPic;

    private String status;

    private List<Movie> movieList;

    @TableField(exist = false)
    private MovieRole role;

}
