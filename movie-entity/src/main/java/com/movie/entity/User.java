package com.movie.entity;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author yjc
 * @since 2021-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 账户
     */
    private Integer id;

    /**
     * 密码
     */
    private String phone;

    /**
     * 注册时默认获取，可修改
     */
    private String password;

    /**
     * 性别
     */
    private String account;

    /**
     * 出生日
     */
    private String sex;

    /**
     * 从事行业
     */
    private String birth;

    /**
     * 兴趣
     */
    private String job;

    /**
     * 签名
     */
    private String sign;

    /**
     * 头像
     */
    private String headpic;


}
