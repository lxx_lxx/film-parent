package com.movie.vo;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class PageResultVO<T> {
    @NonNull
    private boolean flag;  //查询状态
    private Long total; //总记录数

    @NonNull
    private String msg;  //查询信息

    private List<T> data;//当前页数据


}

