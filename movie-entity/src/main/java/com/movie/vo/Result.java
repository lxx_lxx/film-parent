package com.movie.vo;

import lombok.*;

/**
 * @Author xsl
 * @Date 2021/3/16 15:57
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class Result<T> {

    @NonNull
    private boolean success;
    @NonNull
    private String message;

    private T data;


}
