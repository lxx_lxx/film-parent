package com.movie.vo;

import com.movie.entity.Country;
import com.movie.entity.Type;
import lombok.Data;

import java.util.List;

@Data
public class SearchResultVo extends PageResultVO {
    private List<String> typeList;
    private List<String> countryList;
}
