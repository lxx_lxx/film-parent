package com.movie;

import com.netflix.client.config.DefaultClientConfigImpl;
import com.netflix.client.config.IClientConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.movie.mapper")
public class FilmsApp {
    public static void main(String[] args) {
        SpringApplication.run(FilmsApp.class,args);
    }

    /**
     * 解决Ribbon的依赖问题
     * @return
     */
    @Bean
    public IClientConfig iClientConfig() {
        return new DefaultClientConfigImpl();
    }
}