package com.movie.controller;


import com.movie.entity.ESMovie;
import com.movie.service.IMovieService;
import com.movie.vo.PageResultVO;
import com.movie.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.PipedReader;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lxx
 * @since 2021-04-13
 */
@RestController
@RequestMapping("/films")
public class MovieController {
    @Autowired
    private IMovieService service;
    @GetMapping("/initMovie")
    public String initMovie(){
        try {
            service.initMovie();
            return "成功";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "失败";
    }

    @PostMapping("/searchMovieByKeyWord")
    public PageResultVO<ESMovie> searchMovieByKeyWord(@RequestBody Map search){
        try {
            PageResultVO<ESMovie> esMoviePageResultVo = service.searchMovieByKeyWord(search);
            return esMoviePageResultVo;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new PageResultVO<ESMovie>(false,"查询失败");
    }

    /**
     * 关键字搜索电影
     * @param searchMap
     * @return
     */
    @PostMapping("/movieKeywordSearch")
    public Result movieKeywordSearch(@RequestBody Map searchMap){
       return service.movieKeywordSearch(searchMap);
    }

    /**
     * 添加电影同步索引库（就是将状态为已下架的改成已上架的）
     */

    @RequestMapping("/audio")
    public Result audio(@RequestParam("movieId") String movieId){
        try {
            return service.audio(movieId);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"上架失败");
        }
    }
}
