package com.movie.controller;


import com.movie.entity.ESStar;
import com.movie.service.IStarService;
import com.movie.vo.PageResultVO;
import com.movie.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lxx
 * @since 2021-04-13
 */
@RestController
@RequestMapping("/star")
public class StarController {

    @Autowired
    private IStarService starService;

    @RequestMapping("/initStar")
    public Result initStar(){
        try {
            starService.initStar();
            return new Result(true,"初始化演员信息成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result(false,"初始化演员信息失败");
    }

    @RequestMapping("/getAllStar")
    public PageResultVO getAllStar(){
        try {
           return starService.getAllStar();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new PageResultVO(false,"查找异常");
    }


    /**
     * 根据条件对影人进行搜索
     */

    @PostMapping("/searchStarByKeyWord")
    public PageResultVO searchStarByKeyWord(@RequestBody Map map){
        try {
            return starService.searchStarByKeyWord(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new PageResultVO(false,"查询失败");
    }

    /**
     * 加载明星的静态化页面
     * @param starId
     */
    @RequestMapping("/getStar")
    public void getStar(String starId){
        try {
            starService.getStarHtml(starId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
