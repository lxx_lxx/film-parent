package com.movie.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import java.util.Date;

@Data
@Document(indexName = "es-movie",  shards = 1, replicas = 0)
public class ESMovie {

    @Id
    private Integer movieId;                        // spuId



    @Field(type = FieldType.Text, analyzer = "ik_max_word",store=true)
    private String movieName;//电影名称 分词

    @Field(type = FieldType.Text,store=true)
    private String moviePic;//电影图片地址

    @Field(type=FieldType.Double,index=true,store=true)
    private Double price;                       // 票价
    @Field(type=FieldType.Keyword,index=true,store=true)
    private String score;                       // 分数

    @Field(type = FieldType.Text,index=true,store=true)
    private String state;                   //电影状态 （0:火热1:即将上映？）

    @Field(type=FieldType.Date,index=true,store=true,format = DateFormat.basic_date_time)
    private Date uploadTime;                        // 上映时间
    @Field(type = FieldType.Integer,index=true,store=true)
    private Integer totalTime;                    // 总时长

    @Field(type=FieldType.Integer,index=true,store=true)
    private Integer mainStat;                        // 主演id

    @Field(type= FieldType.Integer,index=true,store=true)
    private Integer countryId;                        // 国家ID
    @Field(type = FieldType.Text,index=true,store=true)
    private String typeName;                    // 电影类型 分词

    @Field(type=FieldType.Keyword,index=true,store=true)
    private String countryName;                    // 上映地区名称

    @Field(type=FieldType.Text,analyzer = "ik_max_word",index=true,store=true)
    private String startName; //演员名称 分词

    @Field(type=FieldType.Text,index=true,store=true)
    private String sex; //演员性别
    @Field(type=FieldType.Date,index=true,store=true,format = DateFormat.basic_date_time)
    private Date birth; //演员生日

    @Field(type=FieldType.Text,index=true,store=true)
    private String type; //演员类型（导演？演员）

    @Field(type=FieldType.Text,index=true,store=true)
    private String nationlity; //演员国籍

}
