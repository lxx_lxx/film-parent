package com.movie.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;
import java.util.List;

@Data
@Document(indexName = "es-star",  shards = 1, replicas = 0)
public class ESStar {

    @Id
    private Integer starId;                        // spuId

    @Field(type = FieldType.Text,index=true, analyzer = "ik_max_word",store=true)
    private String starName;//演员姓名 分词

    @Field(type = FieldType.Text,index=true,store=true)
    private String englishName;//演员英文姓名

    @Field(type = FieldType.Integer,index=false,store=true)
    private Integer fensNum;//演员粉丝数

    @Field(type = FieldType.Keyword,store=true)
    private String starPic;//演员图片

    @Field(type = FieldType.Keyword,store=true)
    private List<String> mainMovieList;// 演员代表作


    @Field(type = FieldType.Keyword,store=true)
    private String status;// 演员状态（是否上架）

}
