package com.movie.listenES;

import com.movie.entity.Movie;
import com.movie.service.IMovieService;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(consumerGroup = "mh-movie-films",
topic = "films-2-es",selectorExpression = "films-2-es-tag",
consumeMode = ConsumeMode.CONCURRENTLY,
messageModel = MessageModel.CLUSTERING)
public class FilmsESConsumerListener implements RocketMQListener<Movie> {
    @Autowired
    private IMovieService iMovieService;
    @Override
    public void onMessage(Movie movie) {

        try {
            iMovieService.addFilmToES(movie);
            System.out.println("添加成功");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
