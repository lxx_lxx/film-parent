package com.movie.mapper;

import com.movie.entity.Country;
import com.movie.entity.Movie;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.movie.entity.Type;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lxx
 * @since 2021-04-13
 */
@Repository
public interface MovieMapper extends BaseMapper<Movie> {

    List<Movie> initMovie() throws Exception;

    @Select("select * from type ")
    List<Type> selectTypeNameList() throws Exception;

    @Select("select * from country")
    List<Country> selectcountryNameList() throws Exception;


    Movie findMovieById(String movieId) throws Exception;

    @Update("update movie set state=#{state} where movie_id=#{movieId} ")
    void updateState(Movie movie) throws Exception;
}
