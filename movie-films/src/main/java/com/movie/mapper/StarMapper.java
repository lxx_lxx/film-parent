package com.movie.mapper;

import com.movie.entity.Star;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lxx
 * @since 2021-04-13
 */
public interface StarMapper extends BaseMapper<Star> {

    List<Star> initStar() throws Exception;
}
