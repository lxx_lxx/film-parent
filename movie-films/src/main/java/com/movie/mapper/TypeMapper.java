package com.movie.mapper;

import com.movie.entity.Type;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lxx
 * @since 2021-04-13
 */
public interface TypeMapper extends BaseMapper<Type> {

}
