package com.movie.service;

import com.movie.entity.ESMovie;
import com.movie.entity.Movie;
import com.baomidou.mybatisplus.extension.service.IService;
import com.movie.vo.PageResultVO;
import com.movie.vo.Result;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lxx
 * @since 2021-04-13
 */
public interface IMovieService extends IService<Movie> {

    void initMovie() throws Exception;

    PageResultVO<ESMovie> searchMovieByKeyWord(Map search) throws Exception;

    Result movieKeywordSearch(Map searchMap);

    Result audio(String movieId) throws Exception;

    void addFilmToES(Movie movie) throws Exception;
}
