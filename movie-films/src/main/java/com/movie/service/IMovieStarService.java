package com.movie.service;

import com.movie.entity.MovieStar;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lxx
 * @since 2021-04-13
 */
public interface IMovieStarService extends IService<MovieStar> {

}
