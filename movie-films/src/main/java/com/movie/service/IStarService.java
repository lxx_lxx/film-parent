package com.movie.service;

import com.movie.entity.ESStar;
import com.movie.entity.Star;
import com.baomidou.mybatisplus.extension.service.IService;
import com.movie.vo.PageResultVO;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lxx
 * @since 2021-04-13
 */
public interface IStarService extends IService<Star> {

    void initStar() throws Exception;

    PageResultVO<ESStar> searchStarByKeyWord(Map map) throws Exception;

    PageResultVO getAllStar() throws Exception;

    void getStarHtml(String starId) throws Exception;
}
