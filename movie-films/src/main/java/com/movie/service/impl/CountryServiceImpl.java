package com.movie.service.impl;

import com.movie.entity.Country;
import com.movie.mapper.CountryMapper;
import com.movie.service.ICountryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lxx
 * @since 2021-04-13
 */
@Service
public class CountryServiceImpl extends ServiceImpl<CountryMapper, Country> implements ICountryService {

}
