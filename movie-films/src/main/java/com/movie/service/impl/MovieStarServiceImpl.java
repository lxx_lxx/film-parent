package com.movie.service.impl;

import com.movie.entity.MovieStar;
import com.movie.mapper.MovieStarMapper;
import com.movie.service.IMovieStarService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lxx
 * @since 2021-04-13
 */
@Service
public class MovieStarServiceImpl extends ServiceImpl<MovieStarMapper, MovieStar> implements IMovieStarService {

}
