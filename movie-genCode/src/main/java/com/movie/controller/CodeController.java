package com.movie.controller;

import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.builder.ConfigBuilder;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.FileType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * <p>title: com.portal.controller</p>
 * author yjc
 * description:
 */
@RestController
public class CodeController {


    @RequestMapping("code")
    public String code(String project,String module,String tables) {

        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();
        AutoGenerator mpg2 = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        final String projectPath = System.getProperty("user.dir");
        gc.setOutputDir(projectPath + "/"+project+"/src/main/java");
        gc.setAuthor("xsl");
        gc.setOpen(false);
        //数据库表字段为日期类型的直接生成为Date属性类型
        gc.setDateType(DateType.ONLY_DATE);
        // gc.setSwagger2(true); 实体属性 Swagger2 注解
        mpg.setGlobalConfig(gc);

        GlobalConfig gc2 = new GlobalConfig();
        final String projectPath2 = System.getProperty("user.dir");
        gc2.setOutputDir(projectPath2 + "/"+"movie-entity"+"/src/main/java");
        gc2.setAuthor("xsl");
        gc2.setOpen(false);
        //数据库表字段为日期类型的直接生成为Date属性类型
        gc2.setDateType(DateType.ONLY_DATE);
        // gc.setSwagger2(true); 实体属性 Swagger2 注解
        mpg2.setGlobalConfig(gc2);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://192.168.61.131:3306/mh?useUnicode=true&useSSL=false&characterEncoding=utf8");
        // dsc.setSchemaName("public");
        dsc.setDriverName("com.mysql.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("root");
        mpg.setDataSource(dsc);

        DataSourceConfig dsc2 = new DataSourceConfig();
        dsc2.setUrl("jdbc:mysql://192.168.61.131:3306/mh?useUnicode=true&useSSL=false&characterEncoding=utf8");
        // dsc.setSchemaName("public");
        dsc2.setDriverName("com.mysql.jdbc.Driver");
        dsc2.setUsername("root");
        dsc2.setPassword("root");
        mpg2.setDataSource(dsc2);

        // 包配置
        final PackageConfig pc = new PackageConfig();
        pc.setModuleName(module);
        pc.setParent("com.movie");
        mpg.setPackageInfo(pc);

        final PackageConfig pc2 = new PackageConfig();
        pc2.setModuleName(module);
        pc2.setParent("com.movie");
        mpg2.setPackageInfo(pc2);

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };

        InjectionConfig cfg2 = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };

        // 如果模板引擎是 freemarker
        String templatePath = "/templates/mapper.xml.ftl";
        // 如果模板引擎是 velocity
        // String templatePath = "/templates/mapper.xml.vm";

        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<FileOutConfig>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return projectPath +"/"+project+ "/src/main/resources/mapper/" + pc.getModuleName()
                        + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });
//        cfg.setFileCreate(new IFileCreate() {
//            @Override
//            public boolean isCreate(ConfigBuilder configBuilder, FileType fileType, String filePath) {
//                // 判断自定义文件夹是否需要创建
//                checkDir("调用默认方法创建的目录，自定义目录用");
//                if (fileType == FileType.ENTITY) {
//                    // 已经生成 mapper 文件判断存在，不想重新生成返回 false
//                    return !new File(filePath).exists();
//                }
//                // 允许生成模板文件
//                return true;
//            }
//        });
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

//        cfg2.setFileOutConfigList(focList);
//        mpg2.setCfg(cfg2);

        // 配置模板
        TemplateConfig templateConfig = new TemplateConfig();
        TemplateConfig templateConfig2 = new TemplateConfig();

        // 配置自定义输出模板
        //指定自定义模板路径，注意不要带上.ftl/.vm, 会根据使用的模板引擎自动识别
        // templateConfig.setEntity("templates/entity2.java");
        // templateConfig.setService();
        // templateConfig.setController();

        templateConfig.setXml(null);
        templateConfig.setEntity(null);
        mpg.setTemplate(templateConfig);
        templateConfig2.setController(null);
        templateConfig2.setMapper(null);
        templateConfig2.setXml(null);
        templateConfig2.setService(null);
        templateConfig2.setServiceImpl(null);
        mpg2.setTemplate(templateConfig2);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setRestControllerStyle(true);
        // 公共父类
        // 写于父类中的公共字段
//        strategy.setSuperEntityColumns("id");
        strategy.setInclude(tables.split(","));
        strategy.setControllerMappingHyphenStyle(true);
        strategy.setTablePrefix(pc.getModuleName() + "_");
        mpg.setStrategy(strategy);
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();

        StrategyConfig strategy2 = new StrategyConfig();
        strategy2.setNaming(NamingStrategy.underline_to_camel);
        strategy2.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy2.setEntityLombokModel(true);
        strategy2.setInclude(tables.split(","));
        strategy2.setControllerMappingHyphenStyle(true);
        strategy2.setTablePrefix(pc2.getModuleName() + "_");
        mpg2.setStrategy(strategy2);
        mpg2.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg2.execute();

        return "success";
    }

}