package com.movie;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.api.cinema")
@MapperScan("com.movie.mapper")
public class HtmlApp {
    public static void main(String[] args) {
        SpringApplication.run(HtmlApp.class,args);
    }
}
