package com.movie.controller;


import com.api.movie.MovieClients;
import com.movie.service.impl.GenHtmlService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfig;

import java.io.FileWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/genHtml")
public class GenHtmlController {

    @Autowired
    private FreeMarkerConfig freeMarkerConfig;

    @Autowired
    private GenHtmlService genHtmlService;

    @GetMapping("/genMovieHtml")
    public void genMovieHtml(){
        genHtmlService.genMovieHtml();
    }
//    @Autowired
//    private MovieClients movieClients;

//    @Value("${genHtml.sink}")
//    private String goodsHtmlDir;

   /* private void getHtml(String id) {
        try {
            Configuration configuration = freeMarkerConfig.getConfiguration();
            //4.获取模板对象
            Template template = configuration.getTemplate("/电影详情静态化.ftl");
            //5.创建模型数据

            WxbGoods wxbGoods = goodsClient.selectGoodsBySpuId(spuId);
            List<WxbGoodsSku> wxbGoodsSkuList = goodsClient.selectSkuListBySpuId(spuId);
            List<SpecInfo> specs = wxbGoods.getSpecInfoList();
            System.out.println(wxbGoodsSkuList);
            Map map = new HashMap() {{
                put("specs", specs);
                put("skuList", wxbGoodsSkuList);
            }};
            //6.创建输出流(Writer)对象
            FileWriter fileWriter = new FileWriter(goodsHtmlDir + "/" + spuId + ".html");
            //7.输出
            template.process(map, fileWriter);
            //8.关闭
            fileWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/
}
