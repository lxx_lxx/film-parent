package com.movie.mapper;

import com.movie.entity.Movie;
import com.movie.entity.Type;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface GenHtmlMapper {
    Movie selectMovieById(int i);

    List<Type> selectTypeListByMovieId(int i);
}
