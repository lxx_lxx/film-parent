package com.movie.controller;

import com.movie.entity.Movie;
import com.movie.service.IndexService;
import com.movie.vo.PageResultVO;
import com.movie.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/index")
public class IndexController {

    @Autowired
    private IndexService indexService;

    @RequestMapping("/queryHotMovie")
    public PageResultVO queryHotMovie(String state){

        return indexService.queryHotMovie(state);
    }

    @RequestMapping("/queryUploadMovie")
    public Result queryUploadMovie(){

        return indexService.queryUploadMovie();
    }

    @RequestMapping("/queryPopularMovie")
    public Result queryPopularMovie(){

        return indexService.queryPopularMovie();
    }

    @RequestMapping("/queryStar")
    public PageResultVO queryStar(){

        return indexService.queryStar();
    }

    @RequestMapping("/getOrderId")
    public Result getOrderId(){


        return indexService.getOrderId();
    }
}
