package com.movie.service;


import com.movie.entity.Movie;
import com.movie.vo.PageResultVO;
import com.movie.vo.Result;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

public interface IndexService {

    PageResultVO queryHotMovie(String state);

    Result queryUploadMovie();


    Result queryPopularMovie();

    PageResultVO queryStar();

    Result getOrderId();
}
