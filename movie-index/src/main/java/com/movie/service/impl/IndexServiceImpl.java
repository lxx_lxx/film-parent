package com.movie.service.impl;

import com.api.movie.MovieClients;
import com.api.star.SearchStar;
import com.movie.entity.Movie;
import com.movie.service.IndexService;
import com.movie.vo.PageResultVO;
import com.movie.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import utils.IdWorker;
import utils.JsonUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class IndexServiceImpl implements IndexService {

    @Autowired
    private MovieClients movieClients;

    @Autowired
    private StringRedisTemplate template;

    @Override
    public PageResultVO queryHotMovie(String state) {

        BoundHashOperations<String, Object, Object> ops = template.boundHashOps("hot:movie");

        Object hotMovieJson = ops.get("movieId" + state);


        PageResultVO  pageResultVO=new PageResultVO();
        if (StringUtils.isEmpty(hotMovieJson)){
//            redis为空从数据库中查找
            PageResultVO  pageResultVO1=new PageResultVO();
            synchronized (this){

                Object hotMovieJson1 = ops.get("movieId" + state);

                if (StringUtils.isEmpty(hotMovieJson1)){
//                    数据库获取
                    pageResultVO1 = movieClients.queryHotMovie(state);

                    ops.put("movieId" + state, JsonUtils.toString(pageResultVO1));
                }else {
//                    redis获取
                    pageResultVO1 = JsonUtils.toBean((String) hotMovieJson1, PageResultVO.class);
                }
            }
            return pageResultVO;
        }else {
//            从redis中获取
            System.out.println("redis");
            pageResultVO = JsonUtils.toBean((String) hotMovieJson, PageResultVO.class);
            return pageResultVO;
        }
    }

    @Override
    public Result queryUploadMovie() {

        BoundHashOperations<String, Object, Object> ops = template.boundHashOps("hot:upload");
        Object uploadMovieJson = ops.get("uploadMovie");
        List<Movie> movieList=new ArrayList<>();
        if (StringUtils.isEmpty(uploadMovieJson)){
//            redis is null
            List<Movie> movieList1=new ArrayList<>();
            synchronized (this){
                Object uploadMovie2Json = ops.get("uploadMovie");
                if (StringUtils.isEmpty(uploadMovie2Json)){
//                    数据库获取
                    movieList1 = movieClients.queryUploadMovie();

                    ops.put("uploadMovie",JsonUtils.toString(movieList1));
                }else {
//                    redis
                    movieList1 = JsonUtils.toBean((String) uploadMovie2Json, List.class);
                }

            }

            return new Result(true,"success",movieList1);
        }else {
            System.out.println("redis upload");
             movieList = JsonUtils.toBean((String) uploadMovieJson, List.class);
            return new Result(true,"success",movieList);
        }
    }

    @Override
    public Result queryPopularMovie() {

        BoundHashOperations<String, Object, Object> ops = template.boundHashOps("hot:popular");

        Object popularMovieJson = ops.get("popularMovie");
        List<Movie> movieList=new ArrayList<>();
        if (StringUtils.isEmpty(popularMovieJson)){
//            redis is null
            List<Movie> movieList1=new ArrayList<>();
            synchronized (this){

                Object popularMovie2Json = ops.get("popularMovie");
                if (StringUtils.isEmpty(popularMovie2Json)){
                    movieList1 = movieClients.queryPopularMovie();
                    ops.put("popularMovie",JsonUtils.toString(movieList1));
                }else {
                    movieList1 = JsonUtils.toBean((String) popularMovie2Json, List.class);
                }
            }
            return new Result(true,"success",movieList1);
        }else {
//          redis取
            movieList = JsonUtils.toBean((String) popularMovieJson, List.class);
            return new Result(true,"success",movieList);
        }

    }
    @Autowired
    private SearchStar searchStar;

    @Override
    public PageResultVO queryStar() {


        return searchStar.getAllStar();
    }

    @Autowired
    private IdWorker idWorker;

    @Override
    public Result getOrderId() {


        return new Result(true,"success",idWorker.nextId()+"");
    }

}
