package com.movie.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.movie.entity.Movie;
import com.movie.service.IMovieService;
import com.movie.vo.PageResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;


import java.util.List;


/**
 * <p>
 *  前端控制器
 * </p>
 *
<<<<<<< HEAD
 * @author yjc
=======
 * @author lxx
>>>>>>> f5fb2a12c1ab10d37bcbd8ae8ab1cfc500f61a1f
 * @since 2021-04-13
 */
@RestController
@RequestMapping("/movie")
public class MovieController {

    @Autowired
    private IMovieService iMovieService;

    @RequestMapping("/queryHotMovie")
    public PageResultVO queryHotMovie(String state){

        IPage<Movie> iPage=new Page<>(1,8,true);

        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("state",state);

        queryWrapper.orderByDesc("ticket");

        IPage<Movie> pageInfo = iMovieService.page(iPage, queryWrapper);



        return new PageResultVO(true,pageInfo.getTotal(),"success",pageInfo.getRecords());
    }

    @RequestMapping("/queryUploadMovie")
    public List<Movie> queryUploadMovie(){

        return iMovieService.queryUploadMovie();
    }

    @RequestMapping("/queryPopularMovie")
    public List<Movie> queryPopularMovie(){

        return iMovieService.queryPopularMovie();
    }
}
