package com.movie.service;

import com.movie.entity.Movie;
import com.baomidou.mybatisplus.extension.service.IService;
import com.movie.vo.PageResultVO;


import java.util.List;


/**
 * <p>
 *  服务类
 * </p>
 *
<<<<<<< HEAD
 * @author yjc
=======
 * @author lxx
>>>>>>> f5fb2a12c1ab10d37bcbd8ae8ab1cfc500f61a1f
 * @since 2021-04-13
 */
public interface IMovieService extends IService<Movie> {
    List<Movie> queryUploadMovie();

    List<Movie> queryPopularMovie();
}
