package com.movie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.movie.entity.Movie;
import com.movie.mapper.MovieMapper;
import com.movie.service.IMovieService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.movie.vo.PageResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
<<<<<<< HEAD
 * @author yjc
=======
 * @author lxx
>>>>>>> f5fb2a12c1ab10d37bcbd8ae8ab1cfc500f61a1f
 * @since 2021-04-13
 */
@Service
public class MovieServiceImpl extends ServiceImpl<MovieMapper, Movie> implements IMovieService {


    @Override
    public List<Movie> queryUploadMovie() {

        QueryWrapper queryWrapper=new QueryWrapper();

        queryWrapper.eq("state",1);
        queryWrapper.orderByDesc("want_num");


        return this.baseMapper.selectList(queryWrapper);
    }

    @Autowired
    private MovieMapper movieMapper;

    @Override
    public List<Movie> queryPopularMovie() {

        return movieMapper.selectPopularMovie();
    }
}
