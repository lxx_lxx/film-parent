package com.movie.controller;

import com.movie.servie.IKeywordService;
import com.movie.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
@RequestMapping("/keyword")
public class KeyWordController {

    @Autowired
    private IKeywordService ikeywordService;

    @PostMapping("/movieKeywordSearch")
    public Result movieKeyWordSearch(@RequestBody Map searchMap) {

        return ikeywordService.movieKeywordSearch(searchMap);
    }

    @PostMapping("/cinemaKeywordSearch")
    public Result cinemaKeywordSearch(@RequestBody Map searchMap) {
        System.out.println(searchMap);
        return ikeywordService.cinemaKeywordSearch(searchMap);
    }



}
