package com.movie.servie;

import com.movie.vo.Result;

import java.util.Map;

public interface IKeywordService {
    Result movieKeywordSearch(Map searchMap);

    Result cinemaKeywordSearch(Map searchMap);
}
