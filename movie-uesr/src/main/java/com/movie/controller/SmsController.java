package com.movie.controller;

import com.movie.service.SmsService;
import com.movie.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user/sms")
public class SmsController {

    @Autowired
    private SmsService smsService;

    @RequestMapping("/sendSms/{phone}")
    public Result sendSms(@PathVariable String phone){


        return smsService.sendMsg(phone);
    }

    @RequestMapping("/login")
    public Result login(String phone,String code){

        return smsService.verifyCode(phone, code);
    }
}
