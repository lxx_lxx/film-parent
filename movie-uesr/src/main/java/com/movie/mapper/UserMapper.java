package com.movie.mapper;

import com.movie.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yjc
 * @since 2021-04-16
 */
public interface UserMapper extends BaseMapper<User> {

}
