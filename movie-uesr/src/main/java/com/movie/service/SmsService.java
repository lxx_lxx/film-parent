package com.movie.service;


import com.movie.vo.Result;

public interface SmsService {

    Result sendMsg(String phone);

    Result verifyCode(String phone, String code);
}
