package utils;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


@Component
public class SmsUtil {

//    @Autowired
//    private CodeService codeService;

    static final String accessKeyId ;
    static final String accessKeySecret ;
    static {
        InputStream inputStream = SmsUtil.class.getClassLoader().getResourceAsStream("sms.properties");
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        accessKeyId = properties.getProperty("accessKeyId");
        accessKeySecret = properties.getProperty("accessKeySecret");
    }


    public  static void sendSms(String phoneNum , HttpSession session) throws Exception {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", phoneNum);//设置手机号
        request.putQueryParameter("SignName", "问道学院");//设置短信签名
        request.putQueryParameter("TemplateCode", "SMS_209190607");//设置短信模板


        /*StringBuffer randomNumStr = new StringBuffer();
        for (int i = 0; i < 4; i++) {//产生随机验证码
            randomNumStr.append(new Random().nextInt(10));
        }
        System.out.println(randomNumStr.toString());*/
        String verifyCode = GenerateSmsCode.generateVerifyCode(6, "0123456789");//产生随机验证码
//        session.setAttribute("randomNum",randomNumStr);
        /*//将短信验证码,与手机号存入数据库
        //操作可能是insert,也可能是update.所以要根据手机号判断
        codeService.operateCode(phoneNum,randomNumStr.toString());*/


        request.putQueryParameter("TemplateParam", "{\"code\":\""+verifyCode+"\"}");//设置验证码
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}